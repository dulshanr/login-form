<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
  header("location: welcome.php");
  exit;
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = "";
$lusername_err = $lpassword_err = "";
$username_err = $password_err = "";
 $confirm_password = "";
$confirm_password_err = "";
// Processing form data when form is submitted
//log
if($_SERVER["REQUEST_METHOD"] == "POST"){
    if(isset($_POST['formset'])){

        switch ($_POST['formset']) {
            case "log":
                // Check if username is empty
                if(empty(trim($_POST["username"]))){
                    $lusername_err = "Please enter username.";
                } else{
                    $lusername = trim($_POST["username"]);
                    $username = trim($_POST["username"]);

                }

                // Check if password is empty
                if(empty(trim($_POST["password"]))){
                    $lpassword_err = "Please enter your password.";
                } else{
                    $lpassword = trim($_POST["password"]);
                    $password = trim($_POST["password"]);
                }

                // Validate credentials
                if(empty($username_err) && empty($password_err)) {
                    // Prepare a select statement
                    $sql = "SELECT id, username, password FROM users WHERE username = ?";

                    if ($stmt = mysqli_prepare($link, $sql)) {
                        // Bind variables to the prepared statement as parameters
                        mysqli_stmt_bind_param($stmt, "s", $param_username);

                        // Set parameters
                        $param_username = $username;

                        // Attempt to execute the prepared statement
                        if (mysqli_stmt_execute($stmt)) {
                            // Store result
                            mysqli_stmt_store_result($stmt);

                            // Check if username exists, if yes then verify password
                            if (mysqli_stmt_num_rows($stmt) == 1) {
                                // Bind result variables
                                mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                                if (mysqli_stmt_fetch($stmt)) {
                                    if (password_verify($password, $hashed_password)) {
                                        // Password is correct, so start a new session
                                        session_start();

                                        // Store data in session variables
                                        $_SESSION["loggedin"] = true;
                                        $_SESSION["id"] = $id;
                                        $_SESSION["username"] = $username;

                                        // Redirect user to welcome page
                                        header("location: welcome.php");
                                    } else {
                                        // Display an error message if password is not valid
                                        $lpassword_err = "The password you entered was not valid.";
                                    }
                                }
                            } else {
                                // Display an error message if username doesn't exist

                                $sql = "SELECT id, email, password FROM users WHERE email = ?";

                                if ($stmt = mysqli_prepare($link, $sql)) {
                                    // Bind variables to the prepared statement as parameters
                                    mysqli_stmt_bind_param($stmt, "s", $param_username);

                                    // Set parameters
                                    $param_username = $username;

                                    // Attempt to execute the prepared statement
                                    if (mysqli_stmt_execute($stmt)) {
                                        // Store result
                                        mysqli_stmt_store_result($stmt);

                                        if (mysqli_stmt_num_rows($stmt) == 1) {
                                            // Bind result variables
                                            mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                                            if (mysqli_stmt_fetch($stmt)) {
                                                if (password_verify($password, $hashed_password)) {
                                                    // Password is correct, so start a new session
                                                    session_start();

                                                    // Store data in session variables
                                                    $_SESSION["loggedin"] = true;
                                                    $_SESSION["id"] = $id;
                                                    $_SESSION["username"] = $username;

                                                    // Redirect user to welcome page
                                                    header("location: welcome.php");
                                                } else {
                                                    // Display an error message if password is not valid
                                                    $lpassword_err = "The password you entered was not valid.";
                                                }
                                            }


                                        } else {
                                            $lusername_err = "No account found with that username.";
                                        }
                                    }


                                }
                            }
                            // Close statement
                            mysqli_stmt_close($stmt);
                        }
                    }
                }
                // Close connection
                mysqli_close($link);
                break;
            case "reg":
                // Validate username
                if(empty(trim($_POST["username"]))){
                    $username_err = "Please enter a username.";
                } else{
                    // Prepare a select statement
                    $sql = "SELECT id FROM users WHERE username = ?";

                    if($stmt = mysqli_prepare($link, $sql)){
                        // Bind variables to the prepared statement as parameters
                        mysqli_stmt_bind_param($stmt, "s", $param_username);

                        // Set parameters
                        $param_username = trim($_POST["username"]);

                        // Attempt to execute the prepared statement
                        if(mysqli_stmt_execute($stmt)){
                            /* store result */
                            mysqli_stmt_store_result($stmt);

                            if(mysqli_stmt_num_rows($stmt) == 1){
                                $username_err = "This username is already taken.";
                            } else{
                                $username = trim($_POST["username"]);
                            }
                        } else{
                            echo "Oops! Something went wrong. Please try again later.";
                        }

                        // Close statement
                        mysqli_stmt_close($stmt);
                    }
                }

                // Validate password
                if(empty(trim($_POST["password"]))){
                    $password_err = "Please enter a password.";
                } elseif(strlen(trim($_POST["password"])) < 6){
                    $password_err = "Password must have least 6 characters.";
                } else{
                    $password = trim($_POST["password"]);
                }

                // Validate confirm password
                if(empty(trim($_POST["confirm_password"]))){
                    $confirm_password_err = "Please enter email.";
                } else{
                    $confirm_password = trim($_POST["confirm_password"]);
                    if (!filter_var($confirm_password, FILTER_VALIDATE_EMAIL)) {
                        $emailErr = "Invalid email format";
                    }
                }

                if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){

                    // Prepare an insert statement
                    $sql = "INSERT INTO users (username, email, password) VALUES (?, ?, ?)";

                    if($stmt = mysqli_prepare($link, $sql)){
                        // Bind variables to the prepared statement as parameters
                        mysqli_stmt_bind_param($stmt, "sss", $param_username, $confirm_password, $param_password);

                        // Set parameters
                        $param_username = $username;
                        $confirm_password = trim($_POST["confirm_password"]);
                        $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash

                        // Attempt to execute the prepared statement
                        if(mysqli_stmt_execute($stmt)){
                            // Redirect to login page
                            header("location: login.php");
                        } else{
                            echo "Something went wrong. Please try again later.";
                        }

                        // Close statement
                        mysqli_stmt_close($stmt);
                    }
                }

                // Close connection
                mysqli_close($link);
                break;
        }
    }

}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
<!--    <meta charset="UTF-8">-->
<!--    <title>Login</title>-->
<!--    <style type="text/css">-->
<!--        body{ font: 14px sans-serif; }-->
<!--        .wrapper{ width: 350px; padding: 20px; }-->
<!--    </style>-->
    <title>login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/overwrite.css">
    <!--===============================================================================================-->




</head>
<body class="animsition">


<!--header-->
    <header class="header1">
        <div class="container-menu-header">

            <div class="wrap_header"> <a href="index.html" class="logo"> <img src="images/logo2.png" alt="IMG-LOGO">
                </a>
                <div class="wrap_menu">
                    <nav class="menu">
                        <ul class="main_menu">
                            <li class="sale-noti"> <a href="index.html">Home</a></li>
                            <li> <a href="product1.html" data-toggle="modal" data-target=".bd-product-modal">products</a>

                            </li>
                            <li> <a href="design1.html" data-toggle="modal" data-target=".bd-Designs-modal">Designs</a>

                            </li>
                            <li> <a href="my-ideas.html">MY Ideas</a></li>
                            <li> <a href="about.html">About</a></li>
                            <li> <a href="contact.html">Contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="header-icons">

                    <div class="header-wrapicon2">

                        <a class="header-icon1 js-show-header-dropdown sign-in" href="login.html"><i class="fa fa-user" aria-hidden="true"></i> Sign In </a>
                        <!--
                                         <div class="header-cart header-dropdown header-cart-login">
                                             <ul class="header-cart-wrapitem login">


                                                 <li class="header-cart-item">
                                                     <a href="Order.html"> My orders </a>
                                                 </li>
                                                 <li class="header-cart-item">
                                                     <a href="account-settings.html"> Account Settings </a>
                                                 </li>
                                                 <li class="header-cart-item">
                                                     <a href="#">sign out </a>
                                                 </li>
                                             </ul>


                                         </div>
                 -->
                    </div>

                    <span class="linedivide1"></span>
                    <div class="header-wrapicon2">

                        <a class="header-icon1 js-show-header-dropdown" href="cart.html"><i class="fa fa-shopping-cart" aria-hidden="true"></i> </a>

                        <span class="header-icons-noti">0</span>

                    </div>

                </div>
            </div>
        </div>
        <div class="wrap_header_mobile">
            <a href="index.html" class="logo-mobile"> <img src="images/logo2.png" alt="IMG-LOGO"> </a>
            <div class="btn-show-menu">
                <div class="header-icons-mobile">
                    <a href="login.html" class="header-wrapicon1 dis-block">
                        <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                    </a>
                    <span class="linedivide2"></span>
                    <div class="header-wrapicon2">

                        <a class="header-icon1 js-show-header-dropdown" href="cart.html"><i class="fa fa-shopping-cart" aria-hidden="true"></i> </a>

                        <span class="header-icons-noti">0</span>

                    </div>
                </div>
                <div class="btn-show-menu-mobile hamburger hamburger--squeeze"> <span class="hamburger-box"> <span class="hamburger-inner"></span> </span></div>
            </div>
        </div>
        <div class="wrap-side-menu">
            <nav class="side-menu">
                <ul class="main-menu">
                    <li class="item-menu-mobile"> <a href="index.html">Home</a></li>


                    <li class="item-menu-mobile"> <a href="product.html">products</a>

                    </li>
                    <li class="item-menu-mobile"><a href="design.html">Designs</a>
                    </li>
                    <li class="item-menu-mobile"> <a href="my-ideas.html">MY Ideas</a></li>
                    <li class="item-menu-mobile"> <a href="about.html">About</a></li>
                    <li class="item-menu-mobile"> <a href="contact.html">Contact</a></li>

                </ul>
            </nav>
        </div>
    </header>
    <!--content-->
    <section class="bgwhite p-b-60 mt-75">
        <div class="container">
            <div class="row justify-center m-b-20">

                <div class="col-md-12">
                    <div class="err-mg" style="display:<?php echo (!empty($lusername_err)) ? 'block' : 'none'; ?>;"><lable><?php echo $lusername_err; ?></lable></div>
                    <div class="err-mg" style="display:<?php echo (!empty($lpassword_err)) ? 'block' : 'none'; ?>;"><lable><?php echo $lpassword_err; ?></lable></div>
                    <div class="err-mg" style="display:<?php echo (!empty($password_err)) ? 'block' : 'none'; ?>;"><lable><?php echo $password_err; ?></lable></div>
                    <div class="err-mg" style="display:<?php echo (!empty($username_err)) ? 'block' : 'none'; ?>;"><lable><?php echo $username_err; ?></lable></div>

                    <ul class="nav nav-pills pills-dark mb-3 justify-center" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <p class="f-bold text-center"> Already have an account .</p>
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Login

                            </a>

                        </li>
                        <li class="nav-item">
                            <p class="f-bold text-center"> Don't have an account yet ?</p>
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Register</a>

                        </li>

                    </ul>
                </div>

            </div>

            <div class="row ">
                <div class="tab-content col-md-12" id="pills-tabContent">
                    <div class="  tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row  ">
                            <div class="col-lg-3"> </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 p-b-30 bo-r">
                                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="leave-comment">

                                    <input type="hidden" name="formset" value="log">

                                    <label> Username or Email</label>
                                    <div class="bo4 size15 m-b-20 <?php echo (!empty($lusername_err)) ? 'has-error' : ''; ?>">
                                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="username" value="">
                                        <span class="help-block"><?php echo $lusername_err; ?></span>
                                    </div>

                                    <label> Password</label>
                                    <div class="bo4 size15 m-b-20 <?php echo (!empty($lpassword_err)) ? 'has-error' : ''; ?>">
                                        <input class="sizefull s-text7 p-l-22 p-r-22" type="Password" name="password">
                                        <span class="help-block"><?php echo $lpassword_err; ?></span>
                                    </div>

                                    <label class="container-checkmark ">
                                        Keep me signed in
                                        <input type="checkbox" checked="checked">
                                        <span class="checkmark"></span>
                                    </label>


                                    <div class="">
                                        <!-- Button -->
                                        <button type="submit" class="flex-c-m size2 bg1 bo-rad-3 hov1  trans-0-4 btn-primary m-b-10">Sign In</button>
                                        <a class="text-primary Forgot-pw f-bold" href="#" data-toggle="modal" data-target=".bd-create-modal">Forgot your password? <span> click here</span></a>
                                    </div>





                                </form>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 p-b-30  ">
                                <form class="leave-comment">


                                    <div class="login-box">
                                        <a href="#" class="social-button" id="google-connect"> <span>Connect with Google</span></a>
                                        <a href="#" class="social-button" id="twitter-connect"> <span>Connect with Twitter </span></a>
                                        <a href="#" class="social-button" id="facebook-connect"> <span>Connect with Facebook</span></a>

                                    </div>

                                </form>
                            </div>
                            <div class="col-lg-3"> </div>

                        </div>
                    </div>

<!--                    reg form-->
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="row ">
                            <div class="col-md-3"> </div>
                            <div class="col-md-3 p-b-30 bo-r">
                                <form class="leave-comment" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                                    <input type="hidden" name="formset" value="reg">
                                    <label> name </label>
                                    <div class="bo4  size15 m-b-20 <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>" >
                                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="username">
                                        <span class="help-block"><?php echo $username_err; ?></span>
                                    </div>

                                    <label>email </label>
                                    <div class="bo4 size15 m-b-20 <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                                        <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="confirm_password">
                                        <span class="help-block"><?php echo $confirm_password_err; ?></span>
                                    </div>

                                    <label>Password </label>
                                    <div class="bo4  size15 m-b-20 <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                                        <input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password">
                                        <span class="help-block"><?php echo $password_err; ?></span>
                                    </div>

                                    <p class="text-danger p-b-20 font-14"> it appears that you already have an account with us . Please <a href="login.html" class="text-primary hover-me font-16"> Sign in</a> instead </p>
                                    <div class="">
                                        <button type="submit" class="flex-c-m size2 bg1 bo-rad-3 hov1  trans-0-4 btn-primary" value="Submit">Sign Up</button>
                                    </div>



                                </form>
                            </div>
                            <div class="col-md-3 p-b-30 ">
                                <form class="leave-comment">

                                    <div class="login-box">
                                        <a href="#" class="social-button" id="twitter-connect"> <span>Connect with Twitter </span></a>
                                        <a href="#" class="social-button" id="facebook-connect"> <span>Connect with Facebook</span></a>
                                        <a href="#" class="social-button" id="google-connect"> <span>Connect with Google</span></a>

                                    </div>
                                </form>
                            </div>
                            <div class="col-md-3"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


























    <!--footer-->
    <footer class="bg6 p-t-35 p-b-35 p-l-45 p-r-45">
        <div class="container">
            <div class="row p-b-90">
                <div class="col-md-12 text-center">

                </div>
                <div class="col-md-3  col-sm-6 p-t-20 p-l-15 p-r-15 respon3">
                    <h4 class="s-text12 p-b-30"> CONNECT WITH US</h4>
                    <div>
                        <ul class="social-icons top-i icon-circle icon-rotate list-unstyled list-inline">

                            <li> <a href="" target="_blank"> <i class="fa fa-phone" aria-hidden="true"></i>
                                    <span class="footer-icon"> 1-800-934-1571 </span>
                                </a>
                            </li>
                            <li> <a href="" target="_blank"><i class="fa fa-facebook fa-top"></i>
                                    <span class="footer-icon">facebook </span>
                                </a>
                            </li>
                            <li> <a href="#">
                                    <i class="fa fa-twitter fa-top" target="_blank"></i>
                                    <span class="footer-icon">twitter </span>
                                </a>
                            </li>

                        </ul>

                    </div>
                </div>
                <div class="col-md-3  col-sm-6 p-t-20 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30"> COMPANY</h4>
                    <ul>
                        <li class="p-b-9"> <a href="hwo-to-buy.html" class="s-text7">about us</a></li>
                    </ul>
                </div>

                <div class="col-md-3 col-sm-6 p-t-20 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30"> GET HELP</h4>
                    <ul>
                        <li class="p-b-9"> <a href="Order.html" class="s-text7"> Your Orders </a></li>
                        <li class="p-b-9"> <a href="contact.html" class="s-text7"> Contact Us </a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 p-t-20 p-l-15 p-r-15 respon4">
                    <h4 class="s-text12 p-b-30"> SHARE YOUR IDEAS</h4>
                    <p class="s-text7"> We love hearing from you. Send ideas and comments to </p>
                    <a href="#" class=" s-text7 get-contact">Click here.</a>
                </div>

            </div>
        </div>
    </footer>

    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-arrow-up" aria-hidden="true"></i>
        </span>
    </div>


    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });

        $(".selection-2").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect2')
        });
    </script>
    <!--===============================================================================================-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKFWBqlKAGCeS1rMVoaNlwyayu0e0YRes"></script>
    <script src="js/map-custom.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>
    <script>
        $('.select2').select2()
            .on("select2:open", function() {
                $('.select2-results__options').niceScroll({
                    cursorcolor: "#5fdfe8",
                    cursorwidth: "8px",
                    autohidemode: false,
                    cursorborder: "1px solid #5fdfe8",
                    horizrailenabled: false,
                });
            });
    </script>
    <script>
        $(document).ready(function() {
            function alignModal() {
                var modalDialog = $(this).find(".modal-dialog");

                // Applying the top margin on modal dialog to align it vertically center
                modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
            }
            // Align modal when it is displayed
            $(".modal").on("shown.bs.modal", alignModal);


        });
    </script>
    <script>
        //Call Us
        $(document).ready(function() {
            setTimeout(function() {
                $('#callModal').modal('show');
                $('body').css('padding-right', '0px');
            }, 100000);
        });
    </script>

</body>
</html>